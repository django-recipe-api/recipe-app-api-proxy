    # Recipe App API Proxy

NGNIX proxy for Django Docker API

## Usage

### Enviroment Variables

- 'LISTEN_PORT' - Port to listen on (default: '8000')
- 'APP_HOST' - Hostname of the app to forward requests to
- 'APP_PORT' - Port of the app to forward requests to (default: '9000')
